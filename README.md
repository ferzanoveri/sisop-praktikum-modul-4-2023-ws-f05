# sisop-praktikum-modul-4-2023-WS-F05

| NAMA  | NRP |
| ------------- | ------------- |
| Azhar Abiyu Rasendriya Harun  | 5025211177  |
| Beauty Valen Fajri  | 5025211227 |
| Ferza Noveri  | 5025211097 |

## NOMOR 1
```shell
define MAX_PLAYERS 1000
define MAX_LINE_LENGTH 1000

typedef struct {
    char name[100];
    char club[100];
    int age;
    int potential;
    char photoUrl[100];
} Player;
```
Potongan kode yang diberikan merupakan definisi beberapa preprocessor directives (#define) `define MAX_PLAYERS 1000:` Preprocessor directive ini mendefinisikan sebuah konstanta bernama MAX_PLAYERS dengan nilai 1000. Konstanta ini kemungkinan digunakan untuk membatasi jumlah maksimum pemain yang dapat diolah atau disimpan dalam program dan `define MAX_LINE_LENGTH 1000:` Preprocessor directive ini mendefinisikan sebuah konstanta bernama MAX_LINE_LENGTH dengan nilai 1000. Konstanta ini kemungkinan digunakan untuk membatasi panjang maksimum dari sebuah baris teks atau string yang diolah dalam program.

Untuk `char name[100]:` yakni array karakter dengan ukuran 100 yang menyimpan nama pemain. `char club[100]:` yakni array karakter dengan ukuran 100 yang menyimpan klub pemain.`int age:` merupakan variabel bertipe integer yang menyimpan umur pemain dan `int potential:` merupakan variabel bertipe integer yang menyimpan potensi pemain. serta `char photoUrl[100]:` adalah array karakter dengan ukuran 100 yang menyimpan URL foto pemain.

```shell
int main() {
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
    system("rm fifa-player-stats-database.zip");
```
Potongan kode tersebut merupakan implementasi dari kode `system("kaggle datasets download -d bryanb/fifa-player-stats-database");` dengan menggunakan fungsi `system()` untuk menjalankan perintah kaggle datasets download `-d bryanb/fifa-player-stats-database`. Perintah ini digunakan untuk mengunduh dataset `"fifa-player-stats-database"` dari platform Kaggle. Untuk `system("unzip fifa-player-stats-database.zip");` digunakan untuk menjalankan perintah unzip `fifa-player-stats-database.zip`. Perintah ini digunakan untuk mengekstrak file `ZIP` yang telah diunduh sebelumnya. Sedangkan `system("rm fifa-player-stats-database.zip");` untuk menjalankan perintah `rm fifa-player-stats-database.zip`. Perintah ini digunakan untuk menghapus file ZIP setelah diekstrak.

```shell
 const char *filename = "FIFA23_official_data.csv";
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("File not found.\n");
        return 1;
    }
```
Untuk `const char *filename = "FIFA23_official_data.csv";` mendeklarasikan variabel filename sebagai pointer ke konstanta string `"FIFA23_official_data.csv"`. Variabel ini akan digunakan sebagai nama file yang akan dibuka. Untuk `FILE *file = fopen(filename, "r");` mendeklarasikan pointer file bertipe FILE dan menginisialisasinya dengan hasil dari pemanggilan fungsi `fopen()` untuk membuka file dengan mode `"r"`. File yang akan dibuka adalah file dengan nama yang disimpan dalam variabel filename.

`if (file == NULL)` maka kode tersebut akan melakukan pengecekan apakah pembukaan file berhasil atau tidak. Jika file bernilai `NULL`, artinya file tidak ditemukan atau terjadi kesalahan dalam membukanya. Pada kasus ini, pesan `"File not found."` akan dicetak dan fungsi `main()` akan mengembalikan nilai 1 untuk menandakan adanya kesalahan.

```shell
    char command[MAX_LINE_LENGTH];
    sprintf(command, "awk -F',' 'BEGIN { printf \"%%-8s|%%-16s|%%-4s|%%-40s|%%-12s|%%-5s|%%-7s|%%-9s|%%-18s|%%-12s|%%-9s|%%-7s|%%-8s|%%-7s|%%-8s|%%-8s|%%-12s|%%-15s|%%-10s|%%-7s|%%-8s|%%-13s|%%-13s|%%-7s|%%-7s|%%-16s|%%-14s|%%-20s\\n\", \"ID\", \"Name\", \"Age\", \"Photo\", \"Nationality\", \"Flag\", \"Overall\", \"Potential\", \"Club\", \"Club Logo\", \"Value\", \"Wage\", \"Special\", \"Preferred Foot\", \"Int. Reputation\", \"Weak Foot\", \"Skill Moves\", \"Work Rate\", \"Body Type\", \"Real Face\", \"Position\", \"Joined\", \"Loaned From\", \"Contract Until\", \"Height\", \"Weight\", \"Release Clause\", \"Kit Number\", \"Best Overall Rating\" } NR>1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"%%-8s|%%-16s|%%-4s|%%-40s|%%-12s|%%-5s|%%-7s|%%-9s|%%-18s|%%-12s|%%-9s|%%-7s|%%-8s|%%-7s|%%-8s|%%-8s|%%-12s|%%-15s|%%-10s|%%-7s|%%-8s|%%-13s|%%-13s|%%-7s|%%-7s|%%-16s|%%-14s|%%-20s\\n\", $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28 }' %s", filename);
    system(command);

    fclose(file);

    return 0;
}
```
Untuk `char command[MAX_LINE_LENGTH];` mendeklarasikan array karakter command dengan ukuran `MAX_LINE_LENGTH`. Array ini akan digunakan untuk menyimpan perintah yang akan dijalankan menggunakan fungsi `system()`.`sprintf(command, "awk -F',' 'BEGIN { ... }' %s", filename);` digunakan untuk memformat string perintah yang akan dieksekusi. Pada kasus ini, perintah `awk` akan mencetak baris-baris data yang memenuhi kriteria tertentu ke dalam format yang ditentukan. `%s` akan digantikan dengan nama file yang disimpan dalam variabel filename.

## NOMOR 3
```shell
static const char *dirpath = "/home/tigoyoga/Documents/kuliah/modul-4/soal3/inifolderetc/sisop";
static const char *password = "sisopkeren";
static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeBase64File(const char *filename) {
FILE *file = fopen(filename, "rb");
if (file == NULL) {
printf("Failed to open file: %s\n", filename);
return;
}
```
Kode yang diberikan adalah bagian dari suatu program yang berfungsi untuk melakukan enkripsi file menggunakan metode Base64.

- `dirpath` : Merupakan string yang menyimpan path direktori tempat file akan disimpan. Pathnya adalah `"/home/tigoyoga/Documents/kuliah/modul-4/soal3/inifolderetc/sisop"`.
- `password`: Merupakan string yang menyimpan password yang akan digunakan untuk melakukan enkripsi.
- `base64_table`: Merupakan array karakter yang berisi tabel karakter Base64 yang digunakan dalam proses enkripsi.

Penjelasan fungsi `encodeBase64File(const char *filename)`.Fungsi ini akan mengambil file dengan nama yang diberikan oleh parameter filename dan melakukan enkripsi menggunakan metode Base64. Pertama, fungsi akan membuka file dengan mode "rb" (read binary) menggunakan fungsi fopen(). Jika file gagal dibuka (nilai file pointer file adalah NULL), maka fungsi akan mencetak pesan kesalahan "Failed to open file: [nama file]" dan mengembalikan (return) dari fungsi.Jika file berhasil dibuka, maka proses enkripsi akan dilakukan pada file tersebut.

```shell
fseek(file, 0, SEEK_END);
long file_size = ftell(file);
fseek(file, 0, SEEK_SET);

uint8_t *file_content = (uint8_t *)malloc(file_size);
if (file_content == NULL) {
    printf("Memory allocation failed\n");
    fclose(file);
    return;
}

size_t bytes_read = fread(file_content, 1, file_size, file);
fclose(file);
```
Kode yang diberikan adalah bagian dari fungsi encodeBase64File(const char *filename) yang bertujuan untuk mengambil konten file yang akan dienkripsi.`fseek(file, 0, SEEK_END);` Fungsi ini digunakan untuk memindahkan posisi penunjuk file (file) ke akhir file. Ini dilakukan dengan mengatur offset (pergeseran) dari awal file sebesar 0 (nol) dan mengarahkan posisi penunjuk ke akhir file menggunakan SEEK_END. Dengan melakukan ini, kita mendapatkan ukuran file.`fseek(file, 0, SEEK_SET);` Setelah mendapatkan ukuran file, posisi penunjuk file dikembalikan ke awal file dengan menggunakan SEEK_SET. Ini diperlukan agar kita dapat membaca konten file dari awal.

Untuk `uint8_t *file_content = (uint8_t *)malloc(file_size);` Dilakukan alokasi memori dinamis menggunakan malloc() untuk mengalokasikan ruang memori sebesar file_size bytes. Pointer file_content menunjuk ke lokasi memori yang dialokasikan. uint8_t digunakan untuk mewakili byte individu dalam konten file.Jika `if (file_content == NULL) { ... }:` Periksa apakah alokasi memori berhasil. Jika alokasi memori gagal (pointer file_content bernilai NULL), maka pesan kesalahan "Memory allocation failed" akan dicetak, file akan ditutup menggunakan fclose(file), dan fungsi akan mengembalikan (return).

```shell
if (bytes_read != file_size) {
    printf("Failed to read file: %s\n", filename);
    free(file_content);
    return;
}

uint8_t input[3], output[4];
size_t input_len = 0;
size_t output_len = 0;
size_t i = 0;

file = fopen(filename, "wb");
if (file == NULL) {
    printf("Failed to open file for writing: %s\n", filename);
    free(file_content);
    return;
}

while (i < file_size) {
    input[input_len++] = file_content[i++];
    if (input_len == 3) {
        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, 4, file);

        input_len = 0;
        output_len += 4;
    }
}
```
Kode yang diberikan adalah bagian dari fungsi encodeBase64File(const char *filename) yang bertanggung jawab untuk melakukan enkripsi konten file dalam bentuk Base64 dan menyimpan hasil enkripsi ke file.

Penjelasan kode:
- `if (bytes_read != file_size) { ... }:` Setelah membaca konten file, dilakukan pengecekan apakah jumlah byte yang berhasil dibaca (bytes_read) sama dengan ukuran file (file_size). Jika tidak sama, itu berarti ada kesalahan dalam membaca file. Pesan kesalahan "Failed to read file: [nama file]" akan dicetak, memori yang dialokasikan untuk file_content akan dibebaskan menggunakan free(file_content), dan fungsi akan mengembalikan (return).
- `uint8_t input[3], output[4];` Dua array input dan output digunakan untuk menyimpan data dalam proses enkripsi. input akan menyimpan blok data dengan ukuran 3 byte yang akan dienkripsi, dan output akan menyimpan hasil enkripsi dengan ukuran 4 byte.

Apabila `if (input_len == 3) { ... }:` Jika input sudah berisi 3 byte, maka dilakukan proses enkripsi dan penulisan ke file.
Setiap byte di input diubah menjadi karakter Base64 yang sesuai dan disimpan di output.`fwrite(output, 1, 4, file);` output dengan ukuran 4 byte ditulis ke file menggunakan fwrite().`input_len` diatur kembali menjadi 0 dan `output_len` ditingkatkan sebanyak 4, karena sudah ditulis 4 byte ke file.Setelah semua blok 3-byte selesai diproses, file ditutup menggunakan `fclose(file)`.

```shell
bool is_encoded(const char *name) {
char first_char = name[0];
if (first_char == 'l' || first_char == 'L' ||
first_char == 'u' || first_char == 'U' ||
first_char == 't' || first_char == 'T' ||
first_char == 'h' || first_char == 'H') {
return true;
}
return false;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
int res;
char fpath[1000];

sprintf(fpath, "%s%s", dirpath, path);

res = lstat(fpath, stbuf);

if (res == -1)
    return -errno;

return 0;
}
```
Penjelasan `is_encoded(const char *name):`
Fungsi ini mengambil sebuah string name sebagai parameter dan memeriksa apakah karakter pertama dari string tersebut adalah 'l', 'L', 'u', 'U', 't', 'T', 'h', atau 'H'.Jika karakter pertama sama dengan salah satu dari karakter-karakter tersebut, fungsi akan mengembalikan nilai true, yang menandakan bahwa string tersebut dianggap terenkripsi. Jika tidak, fungsi akan mengembalikan nilai false, yang menandakan bahwa string tersebut tidak terenkripsi. 

Penjelasan `xmp_getattr(const char *path, struct stat *stbuf):`
Fungsi ini mengambil path dari file atau direktori (path) dan sebuah pointer ke struct stat (stbuf) sebagai parameter. Pertama, path absolut dari file atau direktori tersebut dibentuk dengan menggabungkan dirpath (yang sudah didefinisikan sebelumnya) dengan path menggunakan fungsi sprintf(), dan hasilnya disimpan dalam array fpath. Selanjutnya, fungsi lstat() dipanggil dengan fpath sebagai argumen untuk mendapatkan informasi tentang file atau direktori yang ditunjuk oleh fpath. Hasil dari lstat() disimpan dalam stbuf. Jika hasil dari lstat() adalah -1, itu menandakan terjadi kesalahan, dan fungsi akan mengembalikan nilai -errno (kode kesalahan yang sesuai dengan kesalahan yang terjadi dalam bentuk negatif).
Jika tidak ada kesalahan, fungsi akan mengembalikan nilai 0, menandakan bahwa operasi berhasil dilakukan.

```shell
void modify_filename(char *name)
{
for (int i = 0; name[i]; i++)
{
name[i] = tolower(name[i]);
}
}
```
Fungsi ini mengambil sebuah string name sebagai parameter dan melakukan modifikasi pada string tersebut. Digunakan sebuah loop for untuk iterasi melalui setiap karakter dalam string name. Pada setiap iterasi, name[i] (karakter pada indeks i dalam string) diubah menjadi lowercase menggunakan fungsi tolower() dari library C. Dengan melakukan modifikasi ini, setiap karakter dalam string name akan diubah menjadi lowercase.
Setelah loop selesai, nama file akan termodifikasi dan mengandung semua karakter dalam lowercase.

```shell
void convert_to_binary(char *name)
{
int len = strlen(name);
char *temp = (char *)malloc((8 * len + len) * sizeof(char)); // Allocate memory for modified string

int index = 0; // Index for the modified string

for (int i = 0; i < len; i++)
{
    char ch = name[i];

    // Convert character to binary string
    for (int j = 7; j >= 0; j--)
    {
        temp[index++] = (ch & 1) + '0'; // Convert bit to character '0' or '1'
        ch >>= 1;                       // Shift right by 1 bit
    }

    if (i != len - 1)
    {
        temp[index++] = ' '; // Add space separator between binary codes
    }
}

temp[index] = '\0'; // Null-terminate the modified string

strcpy(name, temp); // Update the original string with the modified string
free(temp);         // Free the allocated memory
}
```
Fungsi ini mengambil sebuah string name sebagai parameter dan melakukan konversi pada setiap karakter dalam string tersebut. Pertama, variabel len diinisialisasi dengan panjang (jumlah karakter) dari string name menggunakan fungsi strlen(). Dilakukan alokasi memori dinamis menggunakan malloc() untuk mengalokasikan ruang memori sebesar (8 * len + len) * sizeof(char) byte. 8 * len digunakan untuk mengalokasikan ruang untuk karakter biner dari setiap karakter dalam nama file, sedangkan len digunakan untuk mengalokasikan ruang untuk spasi pemisah antara kode biner karakter.

Pada setiap iterasi, karakter name[i] disimpan dalam variabel ch. Dilakukan loop for lagi untuk mengkonversi karakter ch menjadi string biner.
Loop ini berjalan dari j = 7 (bit paling kiri) ke j = 0 (bit paling kanan). Pada setiap iterasi, bit paling kanan dari ch (ch & 1) diubah menjadi karakter '0' atau '1' dengan menambahkan '0' ('0' + (ch & 1)) dan disimpan di dalam temp[index]. Setelah konversi karakter ke string biner selesai, jika karakter bukan merupakan karakter terakhir dalam string name, spasi pemisah (' ') ditambahkan ke dalam temp[index].

```shell
if (strcmp(path, "/") == 0)
{
    path = dirpath;
    sprintf(fpath, "%s", path);
}
else
    sprintf(fpath, "%s%s", dirpath, path);

int res = 0;

DIR *dp;
struct dirent *de;
(void)offset;
(void)fi;
```
Pertama, terdapat kondisi if (strcmp(path, "/") == 0). Kondisi ini digunakan untuk memeriksa apakah path adalah string "/" yang merepresentasikan root directory. Jika kondisi tersebut benar (artinya path adalah root directory).`sprintf(fpath, "%s", path);` digunakan untuk membentuk path absolut dari direktori yang dituju dan hasilnya disimpan dalam fpath. Setelah membentuk fpath, selanjutnya dilakukan `int res = 0;` digunakan untuk menyimpan hasil operasi yang akan datang.`DIR *dp;` adalah pointer yang akan menunjuk ke struktur direktori.
struct dirent *de; adalah pointer yang akan menunjuk ke entri direktori.

```shell
int main(int argc, char *argv[])
{
umask(0);

// Run the FUSE filesystem
int fuse_stat = fuse_main(argc, argv, &xmp_oper, NULL);

return fuse_stat;
}
```
`umask(0);` digunakan untuk mengatur mask umask ke 0. Umask adalah sebuah bitwise mask yang digunakan saat membuat file atau direktori baru untuk mengontrol hak akses yang diberikan pada file atau direktori tersebut. Dengan mengatur umask ke 0, hak akses akan ditentukan secara eksplisit oleh program file system FUSE.

`int fuse_stat = fuse_main(argc, argv, &xmp_oper, NULL);` adalah pemanggilan fungsi fuse_main() yang menjalankan file system FUSE.
argc dan argv digunakan untuk meneruskan argumen dari program ke file system FUSE. `&xmp_oper` adalah pointer ke struct fuse_operations yang berisi definisi operasi-operasi file system FUSE yang akan diimplementasikan oleh program. Hasil dari `fuse_main()` disimpan dalam variabel fuse_stat.

## NOMOR 4
```shell
FILE *logFile;

void logCall(int argc, char *argv[]) {
  time_t currentTime = time(NULL);
  struct tm *localTime = localtime(&currentTime);

  char cmd[1000];
  char level[10];
  sprintf(cmd, "%s", argv[0]);

  if (!strcmp(cmd,"RMDIR") || !strcmp(cmd,"UNLINK")) {
    sprintf(level,"FLAG");
  }
  else {
    sprintf(level,"REPORT");
  }

  for (int i = 1; i < argc; i++) {
    sprintf(cmd + strlen(cmd), "::%s",argv[i]);
  }
```
Potongan kode ini secara keseluruhan digunakan untuk mencatat informasi log berdasarkan argumen yang diberikan dalam fungsi `logCall()`. Informasi log yang dicatat meliputi waktu, tingkat (level), dan perintah yang dijalankan. Untuk `FILE *logFile;` mendeklarasikan pointer logFile bertipe FILE. Pointer ini akan digunakan untuk menunjuk ke file log yang akan digunakan dalam program. Sedangkan `struct tm *localTime = localtime(&currentTime);` mendeklarasikan pointer localTime bertipe `struct tm` dan menginisialisasinya dengan waktu lokal berdasarkan currentTime menggunakan fungsi `localtime()`.

`char cmd[1000];` Array ini akan digunakan untuk menyimpan perintah yang akan dicatat dalam log dan `char level[10];` digunakan untuk menyimpan tingkat (level) dari log, yaitu `"FLAG"` atau `"REPORT"`. Serta `sprintf(cmd, "%s", argv[0]);` digunakan untuk mengisi `cmd` dengan argumen pertama `(argv[0])`, yaitu nama perintah atau program yang sedang berjalan.

Kode tersebut memiliki kondisi `if (!strcmp(cmd,"RMDIR") || !strcmp(cmd,"UNLINK")) { ... } else { ... }:` maka akan melakukan pengecekan apakah `cmd` sama dengan `"RMDIR"` atau `"UNLINK"`. Jika iya, maka level akan diisi dengan string `"FLAG"`. Jika tidak, maka level akan diisi dengan string `"REPORT"`. Untuk code `for (int i = 1; i < argc; i++) { ... }:` melakukan iterasi untuk setiap argumen setelah argumen pertama `(argv[1]` hingga `argv[argc-1])`. Dalam setiap iterasi, menggunakan `sprintf()` untuk menggabungkan argumen ke dalam cmd dengan menggunakan `"::"` sebagai pemisah.

```shell
const char *getSplitFilePath(const char *path, int index) {
  char *splitFilePath = malloc(strlen(path) + 5);
  sprintf(splitFilePath, "%s.%03d", path, index);
  return splitFilePath;
}

char *isModularDir(const char *path) {
  char temp[strlen(path) + 1];
  strcpy(temp, path);
  const char *prefix = "module_";
  int prefixLen = 7;

  const char* delim = "/";
  char *token = strtok((char *) temp, delim);

  while (token != NULL) {
    if (strncmp(token, prefix, prefixLen) == 0) {
      int len = strstr(path, token) - path + strlen(token);
      char *substring = (char *) malloc(len + 1);
      strncpy(substring, path, len);
      substring[len] = '\0';
      return substring;
    }
    token = strtok(NULL, delim);
  }
  return NULL;
}
```
Potongan kode tersebut mengandung dua fungsi terpisah: `getSplitFilePath()` digunakan untuk menghasilkan path file split berdasarkan path file asli dan indeks split serta `isModularDir() digunakan untuk memeriksa apakah suatu direktori merupakan direktori modular dengan memeriksa awalan nama direktori. 

Untuk `const char *getSplitFilePath(const char *path, int index) { ... }:`
- Fungsi ini digunakan untuk mengembalikan string yang berisi path file yang dibagi menjadi bagian-bagian (split file).
- Fungsi menerima dua argumen: `path` yang merupakan path file asli dan index yang merupakan indeks split file.
- Fungsi ini mengalokasikan memori untuk string splitFilePath dengan ukuran yang cukup untuk menyimpan path ditambah dengan ".xxx" (xxx merupakan angka indeks).
- Fungsi `sprintf()` untuk memformat `string splitFilePath` dengan menggunakan path dan index dengan ujungnya fungsi mengembalikan `splitFilePath`.

Untuk `char *isModularDir(const char *path) { ... }:`
- Fungsi ini memeriksa apakah suatu path direktori merupakan direktori modular.
- Fungsi menerima satu argumen: path yang merupakan path direktori yang akan diperiksa.
- Fungsi membuat salinan dari path ke dalam array temp dengan menggunakan fungsi `strcpy()` untuk keperluan manipulasi.
- Fungsi mendefinisikan prefix sebagai string `"module_"` dan `prefixLen` sebagai panjang string `"module_"`.
- Fungsi menggunakan `delim ("/")` sebagai delimiter dan membagi temp menjadi token-token menggunakan fungsi `strtok()`.
- Dalam setiap iterasi, fungsi memeriksa apakah token memiliki awalan yang sama dengan `prefix` menggunakan `strncmp()`.
- Jika ditemukan token dengan awalan yang sama dengan `prefix`, fungsi menghitung panjang substring yang akan diambil dari path, kemudian mengalokasikan memori untuk substring dan mengisinya dengan substring tersebut. Jika ditemukan token yang sesuai, fungsi mengembalikan substring, jika tidak ditemukan, fungsi mengembalikan NULL.

```shell
int isSplitFile(const char *path) {
  const char *suffix = strrchr(path, '.');

  if (suffix == NULL) return 0;
  for (int i = 0; i <= 999; i++) {
    char index[6];
    sprintf(index, ".%.03d", i);
    if (!strcmp(suffix, index)) return 1;
  }
  return 0;
}
```
Fungsi `isSplitFile()` digunakan untuk memeriksa apakah suatu path file merupakan file yang dibagi menjadi bagian-bagian (split file). Dengan `const char *suffix = strrchr(path, '.');` digunakan untuk mencari posisi terakhir dari karakter '.' dalam string path. Fungsi ini mengembalikan pointer ke karakter '.' tersebut atau NULL jika karakter tersebut tidak ditemukan. Pointer ini disimpan dalam variabel suffix. Dan apabila kode tersebut berada di kondisi `if (suffix == NULL) return 0;` maka akan melakukan pengecekan apakah suffix adalah NULL. Jika iya, maka fungsi mengembalikan nilai 0, menandakan bahwa path bukan merupakan split file.

```shell
void splitFile(const char *path) {
  FILE *fin = fopen(path, "rb");
  if (fin == NULL) return;

  fseek(fin, 0, SEEK_END);
  long fileSize = ftell(fin);
  rewind(fin);

  int numOfFiles = (fileSize + SPLIT_SIZE - 1) / SPLIT_SIZE;

  char output[4096];
  FILE *fout;
  size_t bytesRead;
  char buffer[SPLIT_SIZE];

  for (int i = 0; i < numOfFiles; i++) {
    sprintf(output, "%s.%03d", path, i);
    fout = fopen(output, "wb");
    if (fout == NULL) {
      fclose(fin);
      return;
    }
```
Kode tersebut berada di kondisi apabila `if (fin == NULL) return;` akan melakukan pengecekan apakah fin adalah NULL, yang menandakan bahwa file tidak dapat dibuka. Jika iya, maka fungsi langsung mengembalikan. Dengan beberapa ketentuan `fseek()` untuk memindahkan posisi file ke akhir menggunakan `SEEK_END`.`ftell()` untuk mendapatkan ukuran file dalam byte berdasarkan posisi saat ini (posisi akhir file).`rewind()` untuk memindahkan posisi file kembali ke awal. Disambung dengan kode untuk mengeksekusi kondisi tersebut.

Dan `for (int i = 0; i < numOfFiles; i++) { ... }:` akan melakukan iterasi sebanyak `numOfFiles` untuk membagi file menjadi bagian-bagian.`sprintf(output, "%s.%03d", path, i);` digunakan untuk memformat string output dengan menggunakan path dan indeks i dalam format ".xxx" (xxx merupakan angka indeks dengan padding nol jika kurang dari tiga digit). Jadi, fungsi `splitFile()` membuka file input, menghitung jumlah bagian file yang akan dibuat, dan melakukan iterasi untuk membagi file menjadi bagian-bagian dengan menulis data ke file output yang sesuai. Setiap file output diberi nama dengan indeks menggunakan format ".xxx".

```shell
 bytesRead = fread(buffer, 1, SPLIT_SIZE, fin);
    fwrite(buffer, 1, bytesRead, fout);

    fclose(fout);
  }

  fclose(fin);
  remove(path);
}

void combineFiles(const char *path) {
  FILE *fout = fopen(path, "wb");
  if (fout == NULL) {
    perror("fopen");
    return;
  }

  int index = 0;
  const char *splitFilePath = getSplitFilePath(path, index++);
  while (access(splitFilePath, F_OK) == 0) {
    FILE *fs = fopen(splitFilePath, "rb");
    if (fs == NULL) {
      fclose(fout);
      perror("fopen");
      return;
    }
 ```
Untuk Fungsi `splitFile():`
- `bytesRead = fread(buffer, 1, SPLIT_SIZE, fin);` untuk membaca data dari file input (fin) sebanyak SPLIT_SIZE byte, dan menyimpan hasilnya ke dalam buffer. Jumlah byte yang berhasil dibaca disimpan dalam bytesRead.
- `fwrite(buffer, 1, bytesRead, fout);` untuk menulis data yang terbaca (buffer) sebanyak bytesRead byte ke dalam file output (fout).

Untuk Fungsi `combineFiles():`
- `FILE *fout = fopen(path, "wb");` Membuka file output (fout) dengan mode "wb" (write binary) menggunakan `fopen()` dan menyimpan file pointer dalam variabel fout. Jika fopen() mengembalikan NULL, artinya file tidak dapat dibuka, dan fungsi mengeluarkan pesan kesalahan menggunakan perror() dan langsung mengembalikan.
- `const char *splitFilePath = getSplitFilePath(path, index++);` Menggunakan fungsi `getSplitFilePath()` untuk mendapatkan nama file split yang sesuai dengan path dan indeks yang ditentukan (index).
- `while (access(splitFilePath, F_OK) == 0) { ... }:` digunakan untuk memeriksa apakah file split ada atau tidak (F_OK menunjukkan pengecekan keberadaan file).
- `FILE *fs = fopen(splitFilePath, "rb");` Membuka file split (fs) dengan mode "rb" (read binary) menggunakan fopen() dan menyimpan file pointer dalam variabel fs. Jika fopen() mengembalikan NULL, artinya file tidak dapat dibuka, dan fungsi menutup file output (fout), mengeluarkan pesan kesalahan menggunakan perror(), dan langsung mengembalikan.

```shell
char buffer[SPLIT_SIZE];
    size_t bytesRead = fread(buffer, 1, SPLIT_SIZE, fs);
    fwrite(buffer, 1, bytesRead, fout);

    fclose(fs);
    splitFilePath = getSplitFilePath(path, index++);
  }
  index = 0;
  splitFilePath = getSplitFilePath(path, index++);
  while (access(splitFilePath, F_OK) == 0) {
    remove(splitFilePath);
    splitFilePath = getSplitFilePath(path, index++);
  }

  fclose(fout);
}

void demodularize(const char *path) {
  DIR *dir = opendir(path);
  if (dir == NULL) {
    perror("dir");
    return;
  }
```
Penjelasan dari code tersebut yakni 
- `char buffer[SPLIT_SIZE];` = Mendeklarasikan array buffer dengan ukuran SPLIT_SIZE yang digunakan untuk menyimpan data yang dibaca dari file split.
- `size_t bytesRead = fread(buffer, 1, SPLIT_SIZE, fs);` Menggunakan fread() untuk membaca data dari file split (fs) sebanyak SPLIT_SIZE byte, dan menyimpan hasilnya ke dalam buffer. Jumlah byte yang berhasil dibaca disimpan dalam bytesRead.
fwrite(buffer, 1, bytesRead, fout);: Menggunakan fwrite() untuk menulis data yang terbaca (buffer) sebanyak bytesRead byte ke dalam file output (fout).
- `fclose(fs);` Menutup file split (fs) setelah selesai membaca.
- `splitFilePath = getSplitFilePath(path, index++);` untuk mendapatkan nama file split yang sesuai dengan path dan indeks yang ditentukan (index). Kemudian, increment index agar dapat digunakan pada iterasi selanjutnya.
- `while (access(splitFilePath, F_OK) == 0) { ... }:` digunakan untuk memeriksa apakah file split ada atau tidak (F_OK menunjukkan pengecekan keberadaan file).
- `splitFilePath = getSplitFilePath(path, index++);` untuk mendapatkan nama file split selanjutnya berdasarkan path dan indeks yang ditentukan (index). Kemudian, increment index agar dapat digunakan pada iterasi selanjutnya.

```shell
struct dirent* entry;
  while ((entry = readdir(dir)) != NULL) {
    char filePath[4096];
    sprintf(filePath, "%s/%s", path, entry->d_name);

    struct stat st;

    if (stat(filePath, &st) == -1) {
      continue;
    }

    if (S_ISDIR(st.st_mode)) {
      if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

      demodularize(filePath);   
    } else if (S_ISREG(st.st_mode)) {
      int len = strlen(filePath);
      if (len >= 4 && !strcmp(filePath + len - 4, ".000")) {
        char baseName[256];
        strncpy(baseName, filePath, len - 4);
        combineFiles(baseName);
      }
    }
  }
}

void modularize(const char *path) {
  DIR *dir = opendir(path);
  if (dir == NULL) {
    perror("dir");
    return;
  }
```
Potongan kode tersebut adalah implementasi dari fungsi `demodularize()` yang memproses direktori yang berisi file-file split dengan `struct dirent* entry;` Mendeklarasikan pointer entry bertipe struct dirent. struct dirent merupakan struktur data yang berisi informasi tentang entri dalam direktori.`while ((entry = readdir(dir)) != NULL) { ... }:` Melakukan iterasi untuk setiap entri dalam direktori yang telah dibuka (dir). Pada setiap iterasi, entry akan berisi informasi tentang entri berikutnya dalam direktori.

`if (S_ISDIR(st.st_mode)) { ... }:` Memeriksa apakah entri saat ini merupakan direktori dengan menggunakan `S_ISDIR(st.st_mode)`. Dan `else if (S_ISREG(st.st_mode)) { ... }:` Memeriksa apakah entri saat ini merupakan file dengan menggunakan `S_ISREG(st.st_mode)`. Jika ya, maka akan dilakukan pengecekan apakah nama file tersebut merupakan file split dengan ekstensi .000. Jika iya, maka akan dipanggil fungsi combineFiles(baseName) untuk menggabungkan file-file split dengan nama dasar baseName.

```shell
static int op_getattr(const char *path, struct stat *stbuf) {
  int result = lstat(path,stbuf);
  if (result == -1) return -errno;

  char *argv[] = {"GETATTR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return 0;
}
```
Potongan kode tersebut adalah implementasi dari fungsi `op_getattr()`, yang merupakan operasi sistem berkas FUSE untuk mendapatkan atribut dari suatu entri dalam sistem berkas. `static int op_getattr(const char *path, struct stat *stbuf) { ... }:` Deklarasi dan definisi fungsi op_getattr() dengan parameter path yang merupakan path entri yang akan diperiksa atributnya, dan stbuf yang merupakan pointer ke struktur struct stat untuk menyimpan atribut entri. Untuk `int result = lstat(path,stbuf);` untuk mendapatkan atribut dari entri yang diberikan oleh path. lstat() akan mengisi struktur stbuf dengan atribut entri. Nilai kembalian dari lstat() disimpan dalam variabel result.

Dan apabila `if (result == -1) return -errno;` Memeriksa apakah pemanggilan lstat() mengembalikan nilai -1, yang menandakan terjadi kesalahan. Jika iya, fungsi akan mengembalikan kode kesalahan (-errno) ke FUSE agar dapat ditangani dengan benar.`char *argv[] = {"GETATTR", (char *) path}; int argc = sizeof(argv) / sizeof(argv[0]); logCall(argc, argv);` untuk mencatat pemanggilan operasi sistem berkas.Jadi, fungsi `op_getattr() `bertugas untuk mendapatkan atribut dari suatu entri dalam sistem berkas, mencatat pemanggilan operasi tersebut, dan mengembalikan nilai 0 jika berhasil dilakukan.

```shell
static int op_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
  DIR *dp;
  struct dirent *de;
  (void) offset;
  (void) fi;

  dp = opendir(path);

  if (dp == NULL) return -errno;
  while ((de = readdir(dp)) != NULL) {
    size_t len = strlen(de->d_name);
    if (isSplitFile(de->d_name)) {
      if (len >= 4 && !strcmp(de->d_name + len - 4, ".000")) {
        char baseName[256];
        char filePath[4096];
        strncpy(baseName, de->d_name, len - 4);
        baseName[len - 4] = '\0';
        sprintf(filePath, "%s/%s", path, baseName);
        combineFiles(filePath);
      }
    }
    struct stat st;

    memset(&st, 0, sizeof(st));

    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    if (filler(buf, de->d_name, &st, 0)) break;
  }
  closedir(dp);

  char *argv[] = {"READDIR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc,argv);

  return 0;
}
```
Fungsi ini digunakan untuk membaca isi dari suatu direktori yang diidentifikasi oleh path dan mengisi buffer buf dengan entri-entri direktori menggunakan fungsi filler. `static int op_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) { ... }:` Deklarasi dan definisi fungsi `op_readdir()` dengan parameter path yang merupakan path direktori yang akan dibaca, buf yang merupakan buffer untuk menyimpan entri-entri direktori, filler yang merupakan fungsi pengisian buffer, offset yang menentukan posisi pembacaan dalam direktori, dan fi yang merupakan pointer ke struktur struct fuse_file_info.

Untuk `(void) offset; (void) fi;` Menandakan kepada kompiler bahwa parameter offset dan fi tidak digunakan dalam fungsi ini, untuk menghindari munculnya warning "unused parameter". Apabila `if (dp == NULL) return -errno;` maka kode tersebut akan memeriksa apakah terjadi kesalahan saat membuka direktori. Jika dp bernilai NULL, maka akan mengembalikan kode kesalahan yang dihasilkan oleh opendir() menggunakan -errno.`while ((de = readdir(dp)) != NULL) { ... }:` Melakukan iterasi untuk membaca setiap entri dalam direktori menggunakan fungsi readdir(). 

`if (isSplitFile(de->d_name)) { ... }:` Memeriksa apakah entri direktori tersebut merupakan file pecahan (split file) dengan memanggil fungsi isSplitFile(). Jika kondisi tersebut terpenuhi, maka akan dilakukan penggabungan file menggunakan fungsi combineFiles().`st.st_ino = de->d_ino;` menyimpan nomor inode entri direktori ke dalam struktur st dan `if (filler(buf, de->d_name, &st, 0)) break;` memanggil fungsi filler() untuk mengisi buffer buf dengan nama entri dan atributnya yang disimpan dalam struktur st. Jika fungsi filler() mengembalikan nilai non-nol, menandakan buffer telah penuh, maka loop dihentikan.

```shell
static int op_mknod(const char *path, mode_t mode, dev_t rdev) {
  char *argv[] = {"MKNOD", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);
  return 0;
}
```
Potongan kode tersebut adalah implementasi dari fungsi `op_mknod()` dalam sistem berkas FUSE. Fungsi ini digunakan untuk membuat sebuah node (file, device special file, atau FIFO) dengan nama yang diberikan oleh path, mode yang diberikan oleh mode, dan rdev yang digunakan untuk perangkat khusus (jika berlaku). Untuk `static int op_mknod(const char *path, mode_t mode, dev_t rdev) { ... }:` merupakan perangkat khusus yang akan digunakan jika file adalah perangkat khusus. Dan `char *argv[] = {"MKNOD", (char *) path};` Membuat array argv yang berisi argumen-argumen untuk pemanggilan fungsi logCall().`int argc = sizeof(argv) / sizeof(argv[0]);` menghitung jumlah argumen dalam array argv dengan membagi ukuran total array dengan ukuran satu elemen.Fungsi `op_mknod()` dalam implementasi ini hanya melakukan pencatatan pemanggilan fungsi menggunakan `logCall()`, tetapi tidak melakukan operasi sebenarnya untuk membuat node. Implementasi ini mungkin digunakan untuk tujuan debug atau pencatatan aktivitas dalam sistem berkas FUSE.

```shell
static int op_unlink(const char *path) {
  int result = unlink(path);
  if (result == -1) return -errno;

  char *argv[] = {"UNLINK", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);
  return 0;
}
```
Fungsi `op_unlink()` dalam sistem berkas FUSE dengan implementasi ini melakukan penghapusan file menggunakan `unlink()` dan mencatat pemanggilan fungsi menggunakan `logCall()`. Implementasi ini digunakan untuk tujuan debug atau pencatatan aktivitas dalam sistem berkas FUSE.

```shell
static int op_link(const char *to, const char *from) {
  char *argv[] = {"LINK", (char *) to, (char *) from};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);
  return 0;
}
```
Potongan kode tersebut adalah implementasi dari fungsi `op_link()` dalam sistem berkas FUSE. Fungsi `op_link()` dalam implementasi ini melakukan pembuatan hard link antara dua file yang diberikan dan mencatat pemanggilan fungsi menggunakan logCall(). Implementasi ini mungkin digunakan untuk tujuan debug atau pencatatan aktivitas dalam sistem berkas FUSE.

```shell
static int op_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
  (void)fi;

  FILE* fp = fopen(path, "r");
  if (fp == NULL) {
    return -errno;
  }
  if (fseek(fp, offset, SEEK_SET) == -1) {
    fclose(fp);
    return -errno;
  }

  size_t bytesRead = fread(buf, 1, size, fp);

  fclose(fp);
  
  char *argv[] = {"READ", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  logCall(argc, argv);

  return bytesRead;
}
```
Fungsi `op_read` merupakan implementasi dari operasi read dalam sistem berkas menggunakan `FUSE` (Filesystem in Userspace). Dengan demikian, fungsi `op_read` bertanggung jawab untuk membaca data dari file yang ditentukan dan mengembalikan jumlah byte yang berhasil dibaca.

```shell
int main(int  argc, char *argv[]) {
  umask(0);

  char *homeDir = getpwuid(getuid())->pw_dir;

  char logFilePath[256];
  sprintf(logFilePath, "%s/fs_module.log", homeDir);
  logFile = fopen(logFilePath, "w");

  if (logFile == NULL) {
    perror("bruh");
    return 1;
  }

  return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
Dengan demikian, fungsi main bertanggung jawab untuk mengatur umask, menyiapkan file log, dan memanggil fuse_main untuk menjalankan program FUSE dengan operasi-operasi yang telah diimplementasikan.

Fungsi main merupakan fungsi utama dalam program. Pada code tersebut mengatur umask: Memanggil fungsi umask(0) untuk mengatur nilai umask ke 0. Umask digunakan untuk mengontrol izin default pada pembuatan file baru. Dalam konteks ini, mengatur umask ke 0 berarti izin default yang diterapkan pada file baru akan mengikuti izin yang ditentukan oleh sistem. Mendapatkan direktori home: Menggunakan fungsi getpwuid(getuid())->pw_dir untuk mendapatkan path dari direktori home pengguna. Path ini akan digunakan untuk menentukan lokasi file log.
Menyiapkan path file log: Membuat string logFilePath yang berisi path lengkap untuk file log. Formatnya adalah <homeDir>/fs_module.log, di mana homeDir merupakan direktori home pengguna.

Memanggil fungsi fuse_main: Memanggil fungsi fuse_main dengan argumen argc, argv, &xmp_oper, dan NULL. argc dan argv adalah argumen baris perintah yang diteruskan ke program. &xmp_oper adalah pointer ke struktur fuse_operations yang berisi operasi-operasi yang akan diimplementasikan. NULL adalah argumen opsional untuk data pengguna tambahan. Fungsi ini akan menjalankan loop utama FUSE dan menangani semua operasi sistem berkas. Mengembalikan nilai: Setelah selesai menjalankan fuse_main, fungsi main mengembalikan nilai yang diberikan oleh fuse_main, yang biasanya menunjukkan status keluaran dari program.








